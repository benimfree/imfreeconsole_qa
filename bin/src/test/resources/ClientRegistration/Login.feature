# Feature Definition 

Feature: Login
Description: User wants to login

Background: 

Scenario Outline: Logging in on the system
Given I logged in with <username> and <password>
When button is hit
Then I am successfully logged in

Examples:
    | username  |password |
    |           |         |
    |           |         |