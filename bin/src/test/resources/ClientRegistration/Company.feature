#Feature Definition


Feature: Company Info
Description: Admin user can define, update and delete the company information

Background: 
	Given I currently logged in with <username> and <password>

@DefineCompanyInfo
Scenario Outline: Define Company Info
Given I typed in the <company name>
And typed in the <company address>
And typed in the <tin number>
And typed in the <vat percentage>
And typed in the <contact person>
And typed in the <contact number>
And typed in the <website>
And uploaded the <company assets>
When button is hit
Then information provided is successfuly created and saved


Examples:
		| username	|	password	|
		|						|						|

Examples:
		| company name	| company address	| tin number 	| vat percentage	| contact person	| contact number	| website	| company assets |


@EditingCompanyInfo
Scenario Outline: Editing Company Info
Given the <company name> is changed
And the <company address> is changed 
And the <tin number> is changed
And the <vat percentage> is changed
And the <contact person> is changed
And the <contact number> is changed
And the <website> is changed
And the uploaded <company assets> is changed
When button is hit
Then companys info is successfully updated


Examples:
		| username	|	password	|
		|						|						|

Examples:
		| company name	| company address	| tin number 	| vat percentage	| contact person	| contact number	| website	| company assets |


@DeleteCompanyInfo
Scenario: Deleting Company Info
Given the company information is deleted
When the button is hit
Then error message is displayed 

