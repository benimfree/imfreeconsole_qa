#Feature Definition

Feature: Brand Information
Description: Admin user can define the brand information

Background: 
	Given I am currently logged in using my <username> and <password>

Scenario Outline: Defining Brand Information
Given the information of the brand is entered such as <brand name>
And the <brand category>
And the <brand assets>
Then brand info is successfully created

Examples:
    | brand name  | brand category | brand assets |
    | 						|								 |							|
    
    
Scenario Outline: Creating Multiple Brands
Given the company added more than one <brand name>
And the <category>
And the <brand assets>
Then the added brands is successfully created or saved


Examples:
    | brand name  | brand category | brand assets |
    | 						|								 |							|
    