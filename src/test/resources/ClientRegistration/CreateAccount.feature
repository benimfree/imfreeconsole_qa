#Feature Definition for Client Console

@clientconsole
Feature: Create Account
Description: As an admin user, I can create an account with iMFree

Background: 

Scenario Outline: Creating Account
Given I sign up usifng <username> and <password>
Then account is successfully created 

Examples:
    | username  |password |
    |           |         |
    |           |         |
